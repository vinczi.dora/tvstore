import addReducer from './addReducer'

describe('add reducer', () => {
    it('should return the new name on SET_NEW_NAME', () => {
        const name = 'newTvName';
        const nameResult = {name: name};
        const addName = addReducer(
            {},
            {type:'SET_NEW_NAME', name}
            );
        expect(addName).toEqual(nameResult);
    });
});
