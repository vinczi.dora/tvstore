import * as actions from '../constants/actionTypes'

const initialState = {
  isLoading: false
};

const loadingReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.START_LOADING:
      return {
        ...state, isLoading: true
      };
    case actions.STOP_LOADING:
      return {
        ...state, isLoading: false
      };
    default:
      return state
  }
};

export default loadingReducer
