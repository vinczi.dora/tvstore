import * as actions from '../constants/actionTypes'

const initialState = {
  displaySizeInInches: 0,
  displayType: '',
  resolutionK: 0,
  outputs: [],
  name: '',
  itemNo: ''
};

const addReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_NEW_NAME:
      return {...state, name: action.name};
    case actions.SET_NEW_DISPLAY_SIZE:
      return {...state, displaySizeInInches: parseInt(action.displaySize)};
    case actions.SET_NEW_DISPLAY_TYPE:
      return {...state, displayType: action.displayType};
    case actions.SET_NEW_RESOLUTION:
      return {...state, resolutionK: parseInt(action.resolution)};
    case actions.SET_NEW_ID:
      return {...state, itemNo: action.newId};
    case actions.ADD_NEW_VALUE_TO_ARRAY:
      return {...state, outputs: [...state.outputs, action.newValue]};
    case actions.REMOVE_NEW_VALUE_FROM_ARRAY:
      return {...state, outputs: state.outputs.filter(item => item !== action.newValue)};
    case actions.CLEAR_NEW_PROPERTIES:
      return {...initialState};
    case actions.POST_DEVICE_SUCCESS:
      return {...state};
    default:
      return state
  }
};

export default addReducer
