import { combineReducers } from 'redux'
import addReducer from './addReducer'
import listReducer from './listReducer'
import filterReducer from './filterReducer'
import SingleTvReducer from './singleTvReducer'
import loadingReducer from './loadingReducer'
import userReducer from './userReducer'

const rootReducer = combineReducers({
  addReducer,
  listReducer,
  filterReducer,
  SingleTvReducer,
  loadingReducer,
  userReducer
});

export default rootReducer
