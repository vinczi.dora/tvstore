import * as actions from '../constants/actionTypes'
import Cookies from 'js-cookie';

const cookieToken = Cookies.get('token') ? Cookies.get('token') : '';

const initialState = {
  email: '',
  password: '',
  isLoggedIn: cookieToken.length !== 0,
  isRegistered: false,
  error: ''
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_EMAIL:
      return {...state, email: action.email, error: ''};
    case actions.SET_PASSWORD:
      return {...state, password: action.password, error: ''};
    case actions.REGISTRATION_SUCCESS:
      return {
        ...state, email: '', password: '', isLoggedIn: false, isRegistered: true
      };
    case actions.LOGIN_ERROR:
      return {
        ...state, error: 'Incorrect email or password!' 
      };
    case actions.LOGIN_SUCCESS:
      return {
        ...state, email: '', password: '', isLoggedIn: true, isRegistered: false
      };
    case actions.LOGOUT_SUCCESS:
      return {
        ...state, email: '', password: '', isLoggedIn: false
      };
    default:
      return state
  }
};

export default userReducer
