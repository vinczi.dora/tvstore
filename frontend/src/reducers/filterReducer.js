import * as actions from '../constants/actionTypes'

const initialState = {
  tvsFiltered: [],
  error: null,
  searchValue: '',
  searchCategory: '',
  searchType: '',
  numberFilter: false,
  searchValuesMultiple: []
};

const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_SEARCH_VALUE:
      return {...state, searchValue: state.numberFilter ? parseInt(action.filterValue) : action.filterValue};
    case actions.ADD_SEARCH_VALUE_TO_ARRAY:
      return {...state, searchValuesMultiple: [...state.searchValuesMultiple, action.filterValue]};
    case actions.REMOVE_SEARCH_VALUE_FROM_ARRAY:
      return {...state, searchValuesMultiple: state.searchValuesMultiple.filter(item => item !== action.filterValue)};
    case actions.CLEAR_SEARCH_VALUES_ARRAY:
      return {...state, searchValuesMultiple: []};
    case actions.SET_CATEGORY:
      return {...state, searchCategory: action.filterCategory};
    case actions.SET_TYPE:
      return {...state, searchType: action.filterType};
    case actions.SET_NUMBER_FILTER:
      return {...state, numberFilter:true};
    case actions.UNSET_NUMBER_FILTER:
      return {...state, numberFilter: false};
    case actions.CLEAR_FILTER_VALUES:
      return {...state, searchValue: '', searchCategory: '', searchType: '', numberFilter: false};
    case actions.FILTER_DEVICES:
      return {...state,
        tvsFiltered: action.tvsFiltered,
        error: action.tvsFiltered.length === 0 ? 'no results for this filter!' : ''};
    case actions.CLEAR_FILTER:
      return {...state, tvsFiltered: [], error: ''};
    default:
      return state
  }
};

export default filterReducer
