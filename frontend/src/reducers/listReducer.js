import * as actions from '../constants/actionTypes'

const initialState = {
  tvs: []
};

const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_DEVICES_SUCCESS:
      return {
        ...state,
        tvs: action.tvs
      };
    default:
      return state
  }
};

export default listReducer
