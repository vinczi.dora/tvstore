import * as actions from '../constants/actionTypes'

const initialState = {
  singleTv: [],
  error: null
};

const singleTvReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_SINGLE_TV_SUCCESS:
      return {
        ...state,
        singleTv: action.singleTv
      };
    default:
      return state
  }
};

export default singleTvReducer
