import listReducer from './listReducer'

describe('list reducer', () => {
    it('should return the new list on GET_DEVICES_SUCCESS', () => {
        const newTv = {ID: "testID", name: "TestTv"};
        const Tvs = {tvs: newTv};
        const list = listReducer(
            {},
            {type:'GET_DEVICES_SUCCESS', tvs: newTv}
            );
        expect(list).toEqual(Tvs);
    });
});
