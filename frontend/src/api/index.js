import axios from 'axios';
import store from '../store'
import { startLoading, stopLoading } from '../actions/tvActions'

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    timeout: 2000
});

api.interceptors.request.use(function (config) {
    store.dispatch(startLoading());
    return config;
    }, function (error) {
    return Promise.reject(error);
});

api.interceptors.response.use(function (response) {
    store.dispatch(stopLoading());
    return response;
}, function (error) {
    store.dispatch(stopLoading());
    return Promise.reject(error);
});

function getDevices() {
    return api.get(`/tvs`, {withCredentials: true});
}

async function getSingleTv(id) {
    const resp = await api.get(`/tvs/${id}`, {withCredentials: true});
    return resp.data;
}

function addDevice(tv) {
    return api.post(`/tvs`, {...tv}, {withCredentials: true});
}

function postRegistration(userData) {
    return api.post(`/register`, {...userData});
}

function postLogin(userData) {
    return api.post(`/login`, {...userData}, {withCredentials: true}, );
}

function logout() {
    return api.get(`/logout`, {withCredentials: true});
}

export { getDevices, getSingleTv, addDevice, postRegistration, postLogin, logout }
