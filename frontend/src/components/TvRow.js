import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import { listOutputs } from '../actions/helpers/utils'

const TableRow = styled.tr`
    cursor: pointer;
    &:hover {
        background-color: #D1AB7A;
    }
`;

const TableBlock = styled.td`
    border: 1px solid ${props => props.theme === 'red' ? 'red' : 'black'};
    padding: 5px;
    text-align: center;
`;


class TvRow extends Component {
    onItemClick = () => {
        this.props.history.push(`/list/${this.props._id}`)
    };

    render() {
        return (
            <TableRow onClick={() => this.onItemClick()}>
                <TableBlock>{this.props.name}</TableBlock>
                <TableBlock>{this.props.itemNo}</TableBlock>
                <TableBlock>{this.props.displaySizeInInches}</TableBlock>
                <TableBlock>{this.props.displayType}</TableBlock>
                <TableBlock>{this.props.outputs ? listOutputs(this.props.outputs) : ''}</TableBlock>
                <TableBlock>{this.props.resolutionK}</TableBlock>
            </TableRow>
        )
    }
}

export default withRouter(TvRow);