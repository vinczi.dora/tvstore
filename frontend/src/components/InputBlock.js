import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
    margin: 20px 0;
    position: relative;
`;

const InputLabel = styled.label`
    display: block;
    font-size: 15px;
    font-weight: bold;
    margin: 5px 0;
`;

const InputField = styled.input`
    border: 1px solid #C26332;
    display: block;
    height: 36px;
    line-height: 36px;
    padding: 0 10px;
    &:focus {
        outline: none;
    }
`;

const InputBlock = props => (
    <Wrapper>
        <InputLabel htmlFor={props.name}>{props.title}</InputLabel>
        <InputField
            id={props.name}
            value={props.value}
            type={props.type}
            onChange={props.changeProp ? e => props.changeProp(e.target.id, e.target.value) : e => props.onChange(e.target.value)} />
    </Wrapper>
);

export default InputBlock;
