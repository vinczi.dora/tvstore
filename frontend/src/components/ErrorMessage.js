import React from 'react'
import styled from 'styled-components'

const ErrorText = styled.p`
    color: red;
    font-size: 18px;
    font-weight: bold;
    margin: 20px 0;
`;

const ErrorMessage = props => (
    <ErrorText>{props.content}</ErrorText>
);

export default ErrorMessage;
