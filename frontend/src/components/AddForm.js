import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import CheckboxList from './CheckboxList'
import SelectBlock from './SelectBlock'
import InputBlock from './InputBlock'
import IdBlock from './IdBlock'
import Button from './ButtonBlock'
import { createRandomId } from '../actions/helpers/utils'

const AddFormWrapper = styled.div`
    background-color: #D1AB7A;
    max-width: 500px;
    padding: 20px;
`;

class AddForm extends Component {
    constructor () {
        super()
        this.state = {
            isFormCompleted: false,
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.addNew !== this.props.addNew) {
            if(this.props.addNew.name !== '' && this.props.addNew.displaySizeInInches !== 0 &&
                this.props.addNew.displayType !== '' &&
                this.props.addNew.itemNo !== '' &&
                this.props.addNew.outputs.length > 0 &&
                this.props.addNew.resolutionK !== 0) {this.setState({isFormCompleted: true})}
            else {
                this.setState({isFormCompleted: false})
            }
        }
    }

    createId = () => {
        let newId = createRandomId()
        if (this.props.existingIds.includes(newId)) {
            this.createId()
        } else {
            this.props.setNewId(newId);
        }
    };

    handleOutput = checkbox => {
        const value = checkbox.value;
        const checked = checkbox.checked;
        if (checked) {
            this.props.addNewValueToArray(value)
        } else {
            this.props.removeNewValueFromArray(value)
        }
    };

    addDevice = () => {
        this.props.addDevice(this.props.addNew);
        this.props.clearNewProperties();
        this.props.history.push('/list')
    };

    render() {
        return (
            <AddFormWrapper>
                <InputBlock
                    name={'name'}
                    title={'TV name:'}
                    type={'text'}
                    onChange={this.props.setNewName}
                    value={this.props.addNew.name}/>
                <IdBlock onClick={this.createId} content={this.props.addNew.itemNo}/>
                <InputBlock
                    name={'displaySizeInInches'}
                    title={'Display size:'}
                    type={'number'}
                    onChange={this.props.setNewDisplaySize}
                    value={this.props.addNew.displaySizeInInches}/>
                <SelectBlock
                    title={'Display type:'}
                    name={'displayType'}
                    value={this.props.addNew.displayType}
                    content={[
                        {value: '', label: 'Please choose an option'},
                        {value: 'led'},
                        {value: 'plasma'}
                    ]}
                    onChange={this.props.setNewDisplayType}/>
                <CheckboxList title={'Outputs:'} onChange={this.handleOutput} values={this.props.addNew.outputs}/>
                <SelectBlock
                    title={'Resolution:'}
                    name={'resolutionK'}
                    value={this.props.addNew.resolutionK}
                    content={[
                        {value: '', label: 'Please choose an option'},
                        {value: '1'},
                        {value: '2'},
                        {value: '4'},
                        {value: '8'}
                    ]}
                    onChange={this.props.setNewResolution}/>
                <Button disabled={!this.state.isFormCompleted} onClick={this.addDevice} buttonText={'Add me!'} />
            </AddFormWrapper>
        )}
}

export default withRouter(AddForm);