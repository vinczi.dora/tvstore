import React, { Component } from 'react'
import ListContainer from '../containers/ListContainer'
import FilterContainer from '../containers/FilterContainer'

export default class AllTvs extends Component {
    render () {
        return (
            <div>
                <FilterContainer/>
                <ListContainer/>
            </div>
        )
    }
};
