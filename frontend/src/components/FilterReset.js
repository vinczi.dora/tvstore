import React from 'react'
import Button from './ButtonBlock'
import styled from "styled-components";

const Wrapper = styled.div`
    & button {
        background-color: #1b2c3e;
    }
`;

const FilterReset = props => (
    <Wrapper>
        <Button
            onClick={props.onClear}
            disabled={props.tvsFiltered.length === 0}
            buttonText={'Show full list'}>
        </Button>
    </Wrapper>
);

export default FilterReset;
