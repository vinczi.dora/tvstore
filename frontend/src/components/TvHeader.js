import React from 'react'
import styled from 'styled-components'

const HeaderBlock = styled.th`
    background-color: #ddd;
    border: 1px solid black;
    font-weight: bold;
    padding: 5px;
    text-align: center;
`;

const TvHeader = () => (
    <thead>
            <tr>
                    <HeaderBlock>Name</HeaderBlock>
                    <HeaderBlock>ID</HeaderBlock>
                    <HeaderBlock>Display size (inch)</HeaderBlock>
                    <HeaderBlock>Display type</HeaderBlock>
                    <HeaderBlock>Outputs</HeaderBlock>
                    <HeaderBlock>Resolution</HeaderBlock>
            </tr>
    </thead>
);

export default TvHeader
