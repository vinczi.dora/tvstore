import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
    margin: 20px 0;
    position: relative;
`;

const InputLabel = styled.label`
    display: block;
    font-size: 15px;
    font-weight: bold;
    margin: 5px 0;
`;

const InputSelect = styled.select`
    height: 36px;
    line-height: 36px;
    margin: 0 10px 0 0;
`;

const SelectBlock = props => (
    <Wrapper>
        <InputLabel htmlFor={props.name}>{props.title}</InputLabel>
        <InputSelect
            value={props.value}
            name={props.name}
            id={props.name}
            onChange={props.changeProp ? e => props.changeProp(e.target.id, e.target.value) : e => props.onChange(e.target.value)}>
            {props.content.map(item => (
                <option
                    key={item.value}
                    disabled={item.isDisabled ? item.isDisabled : ''}
                    value={item.value}>
                    {item.label ? item.label : item.value}
                </option>)
            )}
        </InputSelect>
    </Wrapper>
);

export default SelectBlock;
