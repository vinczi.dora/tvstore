import React, { Component } from 'react'
import TvHeader from '../components/TvHeader'
import styled from 'styled-components'
import { listOutputs } from '../actions/helpers/utils'

const TableBlock = styled.td`
    border: 1px solid black;
    padding: 5px;
    text-align: center;
`;

export default class SingleTv extends Component {
    componentDidMount() {
        this.props.getSingleTv(this.props.match.params.id)
    }

    render() {
        const { singleTv } = this.props;
        return (
            <div>
                <table>
                    <TvHeader />
                    <tbody>
                    <tr>
                        <TableBlock>{singleTv.name}</TableBlock>
                        <TableBlock>{singleTv.itemNo}</TableBlock>
                        <TableBlock>{singleTv.displaySizeInInches}</TableBlock>
                        <TableBlock>{singleTv.displayType}</TableBlock>
                        <TableBlock>{singleTv.outputs ? listOutputs(singleTv.outputs) : ''}</TableBlock>
                        <TableBlock>{singleTv.resolutionK}</TableBlock>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}
