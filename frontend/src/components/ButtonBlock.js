import React from 'react'
import styled from 'styled-components'

const Button = styled.button`
    background-color: #C26332;
    border: 0;
    border-radius: 20px;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 36px;
    line-height: 36px;
    margin: 20px 20px 20px 0;
    padding: 0 20px;
    text-transform: uppercase;
    &:disabled {
        opacity: .7;
    }
    &.navigateAddButton {
        position: absolute;
        right: 0;
        top: -76px;
        width: 200px;
    }
    &.logoutButton {
        background-color: #222;
        color: #fff;
        position: absolute;
        right: 0;
        top: 0;
        width: 200px;
    }
`;

const ButtonBlock = props => (
    <Button
        className={props.className ? props.className : null}
        onClick={props.onClick}
        disabled={props.disabled}>
        {props.buttonText}
    </Button>
);

export default ButtonBlock;
