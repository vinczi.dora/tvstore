import React, { Component } from 'react'
import styled from 'styled-components'
import InputBlock from './InputBlock'
import SelectBlock from './SelectBlock'
import Button from './ButtonBlock'
import { tvFilteredEqualsTo, tvFilteredContains, tvFilteredSmallerThan, tvFilteredLargerThan}  from '../actions/filterActions'
import { CATEGORY_SELECT_VALUES, TYPE_SELECT_VALUES } from '../constants/selectList'

const Wrapper = styled.div`
    display: flex;
    width: 800px;
    padding: 20px 0 0;
`;

export default class Filter extends Component {
    handleFilterCategory = category => {
        const numberFilters = ['resolutionK', 'displaySizeInInches'];

        if (numberFilters.includes(category)) {
            this.props.setNumberFilter();
            this.props.setCategory(category)
        } else {
            this.props.unsetNumberFilter();
            this.props.setCategory(category)
        }
    };

    onFilter = () => {
        const tvs = this.props.tvs;
        switch (this.props.searchType) {
            case 'contains':
                this.props.filterDevices(tvFilteredContains(tvs, this.props.searchCategory, this.props.searchValue));
                break;
            case 'equalsTo':
                this.props.filterDevices(tvFilteredEqualsTo(tvs, this.props.searchCategory, this.props.searchValue));
                break;
            case 'largerThan':
                this.props.filterDevices(tvFilteredLargerThan(tvs, this.props.searchCategory, this.props.searchValue));
                break;
            case 'smallerThan':
                this.props.filterDevices(tvFilteredSmallerThan(tvs, this.props.searchCategory, this.props.searchValue));
                break;
            default:
                break;
        }
        this.props.clearFilterValues();
    };

    render() {
        return (
            <div>
                <Wrapper>
                    <SelectBlock
                        title={'Which field?'}
                        name={'category'}
                        value={this.props.searchCategory}
                        content={CATEGORY_SELECT_VALUES}
                        onChange={this.handleFilterCategory}/>
                    <SelectBlock
                        title={'Which type?'}
                        name={'types'}
                        value={this.props.searchType}
                        content={TYPE_SELECT_VALUES(this.props.numberFilter)}
                        onChange={this.props.setType}/>
                    <InputBlock
                        name={'value'}
                        title={'What are you looking for?'}
                        type={this.props.numberFilter ? 'number' : 'text'}
                        onChange={this.props.setSearchValue}
                        value={this.props.searchValue}/>
                </Wrapper>
                <Button
                    onClick={this.onFilter}
                    buttonText={'Filter me!'}
                    disabled={
                        this.props.searchValue === '' ||
                        this.props.searchType === '' ||
                        this.props.searchValue === ''}>
                    Filter me!
                </Button>
            </div>
        )
    }
}
