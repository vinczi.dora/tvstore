import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import AllTvs from '../components/AllTvs'
import AddContainer from '../containers/AddContainer'
import SingleTvContainer from '../containers/SingleTvContainer'
import LoginContainer from '../containers/LoginContainer'
import RegisterContainer from '../containers/RegisterContainer'
import NotFound from './NotFound'
import Cookies from 'js-cookie';

export default class App extends Component {
    render () {
        const cookieToken = Cookies.get('token') ? Cookies.get('token') : '';
        const isLoggedIn = cookieToken.length !== 0;
        return (
            <div>
                {isLoggedIn ?
                    <Switch>
                        <Route path='/list/:id' component={SingleTvContainer}/>
                        <Route path='/list' component={AllTvs}/>
                        <Route path='/add' component={AddContainer}/>
                        isLoggedIn ? <Redirect from='/login' to='/list' exact/>
                        <Route path='/login' component={LoginContainer} />
                        <Redirect from='/' to='/list' exact/>
                        <Route component={NotFound}/>
                    </Switch> :
                    <Switch>
                        <Route path='/login' component={LoginContainer}/>
                        <Route path='/register' component={RegisterContainer}/>
                        <Redirect to='/login'/>
                    </Switch>
                }
            </div>
        )
    }
};
