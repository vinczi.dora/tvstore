import React from 'react';
import { render } from 'react-testing-library';
import SingleTv from './SingleTv';

describe('Single tv page', () => {
    const routerProps = {match: { params: {id: 12} } };
    const singleTv = {};
    const getSingleTv = jest.fn();

    afterEach(() => {
        getSingleTv.mockRestore();
    });

   it('should render a single tv page', () => {
       const { container } = render(
           <SingleTv {...routerProps} singleTv={singleTv} getSingleTv={getSingleTv}/>
       );

       expect(container.textContent).toMatchSnapshot("single tv page");
   })

    it('should call  getSingleTv with the passed id on mount', () => {
        const { container } = render(
            <SingleTv {...routerProps} singleTv={singleTv} getSingleTv={getSingleTv}/>
        );

        expect(getSingleTv).toHaveBeenCalledTimes(1);
        expect(getSingleTv).toHaveBeenLastCalledWith(12);
    })
});
