import React, { Component } from 'react'
import Button from './ButtonBlock'
import CheckboxList from './CheckboxList'
import { tvFilteredContainsMultiple }  from '../actions/filterActions'

export default class FilterMultiselect extends Component {
    handleOutput = checkbox => {
        const checked = checkbox.checked;
        if (checked) {
            this.props.addSearchValueToArray(checkbox.value)
        } else {
            this.props.removeSearchValueFromArray(checkbox.value)
        }
    };
    
    onFilter = () => {
        const tvs = this.props.tvs;
        this.props.filterDevices(tvFilteredContainsMultiple(tvs, this.props.searchValuesMultiple));
        this.props.clearSearchValuesArray()
    };

    render() {

        return (
            <div>
                <CheckboxList title={'Which outputs?'} onChange={this.handleOutput} values={this.props.searchValuesMultiple}/>
                <Button
                    buttonText={'Filter me!'}
                    onClick={this.onFilter}
                    disabled={this.props.searchValuesMultiple.length === 0} />
            </div>
        )
    }
}
