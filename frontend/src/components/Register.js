import React, { Component } from 'react'
import styled from 'styled-components'
import Button from './ButtonBlock'
import {withRouter} from "react-router-dom";

const Wrapper = styled.div`
    background-color: #D1AB7A;
    margin: 20px auto;
    padding: 20px;
    text-align: center;
    width: 500px;
`;

const Input = styled.input`
    border: 1px solid #C26332;
    display: block;
    height: 30px;
    line-height: 30px;
    margin: 0 auto 20px;
    padding: 0 10px;
    &:focus {
        outline: none;
    }
`;

class Register extends Component {
        componentDidUpdate(prevProps, prevState) {
                if(prevProps.userData.isRegistered !==this.props.userData.isRegistered){
                        this.redirectToLogin()
                }
        }

        redirectToLogin = () => {
                this.props.history.push(`/login`)
        };

        render() {
                return (
                    <Wrapper>
                            <h2>Register here:</h2>
                            <label htmlFor="registerEmail">Email:</label>
                            <Input onChange={e => this.props.setEmail(e.target.value)} id="registerEmail" type="email" value={this.props.userData.email}/>
                            <label htmlFor="registerPassword">Password:</label>
                            <Input onChange={e => this.props.setPassword(e.target.value)} id="registerPassword" type="password" value={this.props.userData.password}/>
                            <Button buttonText={'Register'} onClick={() => this.props.postRegistration({email: this.props.userData.email, password: this.props.userData.password})} />
                    </Wrapper>
                )}
}

export default withRouter(Register);
