import React,  { Component } from 'react'
import styled from 'styled-components'
import Button from './ButtonBlock'
import {withRouter} from "react-router-dom";

const Wrapper = styled.div`
    background-color: #ECCC84;
    margin: 20px auto;
    padding: 20px;
    text-align: center;
    width: 500px;
`;

const Input = styled.input`
    border: 1px solid #C26332;
    display: block;
    height: 30px;
    line-height: 30px;
    margin: 0 auto 20px;
    padding: 0 10px;
    &:focus {
        outline: none;
    }
`;

const Error = styled.p`
    color: red;
`;

class Login extends Component {
        componentDidUpdate(prevProps, prevState) {
                if(prevProps.userData.isLoggedIn !==this.props.userData.isLoggedIn && this.props.userData.isLoggedIn === true){
                this.redirectToList()
                }
        }

        redirectToList = () => {
                this.props.history.push(`/list`)
        };

        render() {
                return (
                    <Wrapper>
                        <h2>Log in here:</h2>
                        <label htmlFor="loginEmail">Email:</label>
                        <Input id="loginEmail" type="email" value={this.props.userData.email} onChange={e => this.props.setEmail(e.target.value)}/>
                        <label htmlFor="loginPassword">Password:</label>
                        <Input id="loginPassword" type="password" value={this.props.userData.password} onChange={e => this.props.setPassword(e.target.value)}/>
                            {this.props.userData.error ? <Error>{this.props.userData.error}</Error> : ''}
                        <Button buttonText={'Log in'} onClick={() => this.props.postLogin({email: this.props.userData.email, password: this.props.userData.password})}/>
                    </Wrapper>
        )}
}

export default withRouter(Login);
