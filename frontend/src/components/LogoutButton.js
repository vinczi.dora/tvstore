import React, { Component } from 'react'
import Button from './ButtonBlock'
import {withRouter} from "react-router-dom";

class LogoutButton extends Component {
    componentDidUpdate(prevProps, prevState) {
        if(prevProps.isLoggedIn !==this.props.isLoggedIn && this.props.isLoggedIn === false){
            this.redirectToLogin()
        }
    }

    redirectToLogin = () => {
        this.props.history.push(`/login`)
    };

    render() {
        return (
            <div>
                <Button
                    className='logoutButton'
                    onClick={() => this.props.logout()}
                    buttonText={'Log out'} />
            </div>
        )
    }
}

export default withRouter(LogoutButton);