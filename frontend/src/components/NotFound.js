import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
    margin: 20px auto;
    padding: 20px;
    text-align: center;
`;

const Text = styled.span`
  color: #000;
  display: block;
  font-size: 20px;
  font-weight: bold;
`;

const NotFound = props => (
    <Wrapper>
        <Text>Sorry, the page is not found :(</Text>
    </Wrapper>
)

export default NotFound
