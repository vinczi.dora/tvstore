import React from 'react'
import styled from 'styled-components'
import Button from "./ButtonBlock";

const Wrapper = styled.div`
    margin: 20px 0;
    position: relative;
`;

const IdWrapper = styled.div`
    align-items: center;
    display: flex;
`;

const IdContent = styled.p`
    background-color: rgba(256,256,256, .3);
    height: 30px;
    line-height: 30px;
    margin: 0 0 0 5px;
    padding: 0 5px;
    width: 105px;
`;

const InputBlock = props => (
    <Wrapper>
        <IdWrapper>
            <Button onClick={props.onClick} buttonText={'Create a new ID:'}/>
            <IdContent>{props.content}</IdContent>
        </IdWrapper>
    </Wrapper>
);

export default InputBlock;
