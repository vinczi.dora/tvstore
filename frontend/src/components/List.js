import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import TvRow from '../components/TvRow'
import TvHeader from '../components/TvHeader'
import NoResults from '../components/NoResults'
import Button from '../components/ButtonBlock'

const Wrapper = styled.div`
    position: relative;
    width: 1000px;
`;

class List extends Component {
    onAddButtonClick = () => {
        this.props.history.push('/add')
    };
    
    render() {
        return (
            <Wrapper>
                <Button className={'navigateAddButton'} onClick={this.onAddButtonClick} buttonText={'+ Add a new tv!'} />
                {this.props.tvsFiltered.length || this.props.tvs.length ?
                    <div>
                        <table>
                            <TvHeader/>
                            <tbody>
                            {this.props.tvsFiltered.length ?
                                this.props.tvsFiltered.map((tv, id) =>
                                    <TvRow key={id} {...tv} />)
                                : this.props.tvs.map((tv, id) =>
                                    <TvRow key={id} {...tv} />)}
                            </tbody>
                        </table>
                    </div>
                    : <NoResults />}
            </Wrapper>
        )}
}

export default withRouter(List);
