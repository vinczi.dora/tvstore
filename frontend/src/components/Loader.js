import React from 'react'
import styled, { keyframes } from 'styled-components'

const transition = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`

const LoadingAnimation = styled.div`
  align-items: center;
  background: rgba(255,255,255, .85);
  display: flex;
  height: 100vh;
  justify-content: center;
  left: 0;
  position: fixed;
  top: 0;
  width: 100vw;
  z-index: 11;
`

const Rotator = styled.div`
  animation: ${transition} 2s linear infinite;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #D1AB7A;
  height: 80px;
  width: 80px;
`

const Loader = () => (
    <LoadingAnimation>
        <Rotator />
    </LoadingAnimation>
)

export default Loader
