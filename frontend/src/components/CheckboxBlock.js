import React from 'react'
import styled from 'styled-components'

const CheckboxWrapper = styled.div`
    min-width: 150px;
    & input:checked + label {
        color: #C26332;
        font-weight: bold;
    }
`;

const CheckboxBlock = props => (
    <CheckboxWrapper>
        <input type="checkbox" id={props.name} value={props.name} checked={props.values.includes(props.name) ? 'checked' : ''} onChange={e => props.onChange(e.target)}/>
        <label htmlFor={props.name}>{props.name}</label>
    </CheckboxWrapper>
);

export default CheckboxBlock;
