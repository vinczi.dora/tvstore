import React from 'react'
import styled from 'styled-components'
import CheckboxBlock from './CheckboxBlock'
import { OUTPUT_CHECKBOXES } from '../constants/outputList'

const CheckboxesWrapper = styled.div`
    display: flex;
    flex-flow: row wrap;
    margin: 20px 0 0;
    max-width: 600px;
    padding: 20px 0 10px;
    position: relative;
`;

const InputLabel = styled.label`
    font-weight: bold;
    position: absolute;
    top: -5px;
`;


const CheckboxList = props => (
    <CheckboxesWrapper>
        <InputLabel>{props.title}</InputLabel>
        {OUTPUT_CHECKBOXES.map(checkbox => (
            <CheckboxBlock key={checkbox} name={checkbox} onChange={props.onChange} values={props.values} />)
        )}
    </CheckboxesWrapper>
);

export default CheckboxList;
