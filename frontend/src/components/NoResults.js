import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
    background-color: #ECCC84;
    max-width: 300px;
    margin: 20px 0;
    padding: 20px;
`;

const Text = styled.span`
  color: #fff;
  display: block;
  font-size: 14px;
  font-weight: bold;
`;

const NoResults = props => (
    <Wrapper>
        <Text>Sorry, there are no TVs to list :(</Text>
    </Wrapper>
)

export default NoResults
