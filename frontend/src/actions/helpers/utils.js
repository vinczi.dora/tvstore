export const listOutputs = outputs => {
    return Object.keys(outputs).map(i =>
        i < (outputs.length - 1) ? `${outputs[i].toString()}, ` : `${outputs[i].toString()}`
    );
};

export const createRandomId = () => {
    return Math.random().toString(36).substring(2,12).toUpperCase();
};
