import * as actions from '../constants/actionTypes'
import {
    postRegistration as postRegistrationAPI,
    postLogin as postLoginAPI,
    logout as logoutAPI
} from '../api'

const registrationSuccess = () => ({type: actions.REGISTRATION_SUCCESS});

export const postRegistration = userData => dispatch => {
    postRegistrationAPI(userData)
        .then(() => {
            dispatch(registrationSuccess())
        })
};

const loginSuccess = () => ({type: actions.LOGIN_SUCCESS});
const loginError = () => ({type: actions.LOGIN_ERROR});

export const postLogin = userData => dispatch => {
    postLoginAPI(userData)
        .then(response => {
            dispatch(loginSuccess())
        })
        .catch(error => {
            if (error.response.status === 401) {
                dispatch(loginError())    
            }
        })
};

export const logout = () => dispatch => {
    logoutAPI()
        .then(() => {
            dispatch(logoutSuccess())
        })
};

const logoutSuccess = () => ({type: actions.LOGOUT_SUCCESS});

export const setEmail = email => ({type: actions.SET_EMAIL, email});
export const setPassword = password => ({type: actions.SET_PASSWORD, password});
