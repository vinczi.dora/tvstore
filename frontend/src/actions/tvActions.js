import * as actions from '../constants/actionTypes'
import {
    getDevices as getDevicesAPI,
    getSingleTv as getSingleTvAPI
} from '../api'

const getSingleTvSuccess = singleTv => ({type: actions.GET_SINGLE_TV_SUCCESS, singleTv});

export const getSingleTv = id => dispatch => {
    getSingleTvAPI(id)
        .then(response => {
            dispatch(getSingleTvSuccess(response))
        })
};

const getDevicesSuccess = tvs => ({type: actions.GET_DEVICES_SUCCESS, tvs});

export const getDevices = () => dispatch => {
    getDevicesAPI()
        .then(response => {
            dispatch(getDevicesSuccess(response.data))
        })
};

export const startLoading = () => ({type:actions.START_LOADING});
export const stopLoading = () => ({type:actions.STOP_LOADING});
