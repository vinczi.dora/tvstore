import * as actions from '../constants/actionTypes'
import {addDevice as addDeviceAPI} from "../api";
import {getDevices} from "./tvActions";

export const setNewName = name =>  ({type:actions.SET_NEW_NAME, name});
export const setNewDisplaySize = displaySize =>  ({type:actions.SET_NEW_DISPLAY_SIZE, displaySize});
export const setNewDisplayType = displayType =>  ({type:actions.SET_NEW_DISPLAY_TYPE, displayType});
export const setNewResolution = resolution =>  ({type:actions.SET_NEW_RESOLUTION, resolution});
export const setNewId = newId =>  ({type:actions.SET_NEW_ID, newId});
export const addNewValueToArray = newValue =>  ({type:actions.ADD_NEW_VALUE_TO_ARRAY, newValue});
export const removeNewValueFromArray = newValue =>  ({type:actions.REMOVE_NEW_VALUE_FROM_ARRAY, newValue});
export const clearNewProperties = () => ({type:actions.CLEAR_NEW_PROPERTIES});

const postDeviceSuccess = () => ({type: actions.POST_DEVICE_SUCCESS});

export const addDevice = tv => dispatch => {
    addDeviceAPI(tv)
        .then(response => {
            dispatch(postDeviceSuccess(response.data))
            dispatch(getDevices())
        })
};
