import * as actions from '../constants/actionTypes'

export const filterDevices = tvsFiltered => ({type: actions.FILTER_DEVICES, tvsFiltered});
export const clearFilter = () => ({type: actions.CLEAR_FILTER});
export const setSearchValue = filterValue =>  ({type:actions.SET_SEARCH_VALUE, filterValue});
export const setCategory = filterCategory =>  ({type:actions.SET_CATEGORY, filterCategory});
export const setType = filterType =>  ({type:actions.SET_TYPE, filterType});
export const setNumberFilter = () =>  ({type:actions.SET_NUMBER_FILTER});
export const unsetNumberFilter = () =>  ({type:actions.UNSET_NUMBER_FILTER});
export const clearFilterValues = () =>  ({type:actions.CLEAR_FILTER_VALUES});

export const addSearchValueToArray = filterValue =>  ({type:actions.ADD_SEARCH_VALUE_TO_ARRAY, filterValue});
export const removeSearchValueFromArray = filterValue =>  ({type:actions.REMOVE_SEARCH_VALUE_FROM_ARRAY, filterValue});
export const clearSearchValuesArray = () => ({type:actions.CLEAR_SEARCH_VALUES_ARRAY});


export const tvFilteredEqualsTo = (tvs, category, value) => tvs.filter(tv => {
    return tv[category] === value
});

export const tvFilteredContains = (tvs, category, value) => tvs.filter(tv => {
    return tv[category].includes(value.trim())
});

export const tvFilteredSmallerThan = (tvs, category, value) => tvs.filter(tv => {
    return tv[category] < Number(value)
});

export const tvFilteredLargerThan = (tvs, category, value) => tvs.filter(tv => {
    return tv[category] > Number(value)
});

export const tvFilteredContainsMultiple = (tvs, values)  => tvs.filter(tv => {
    for (const value of values) {
        if (tv['outputs'].includes(value) === false) {return false}
    }
    return true
});
