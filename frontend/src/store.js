import { createStore, compose, applyMiddleware } from "redux";
import {  reduxLogger } from './utils/reduxLogger';
import thunk from "redux-thunk";
import rootReducer from "./reducers";

const middleware = [thunk, reduxLogger];

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      })
    : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware)
);

const store = createStore(rootReducer, enhancer);

export default store;
