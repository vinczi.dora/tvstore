export const CATEGORY_SELECT_VALUES = [
    {value: '', label: 'Please choose a category'},
    {value: 'name', label: 'Name'},
    {value: 'itemNo', label: 'ID'},
    {value: 'displayType', label: 'Display types'},
    {value: 'displaySizeInInches', label: 'Display size'},
    {value: 'resolutionK', label: 'Resolution'},
];

export const TYPE_SELECT_VALUES = isNumberFilter => {
    return [
        {value: '', label: 'Please choose a type'},
        {value: 'contains', label: 'contains', isDisabled: isNumberFilter},
        {value: 'equalsTo', label: 'equals to', isDisabled: false},
        {value: 'largerThan', label: 'larger than', isDisabled: !isNumberFilter},
        {value: 'smallerThan', label: 'smaller than', isDisabled: !isNumberFilter},
    ];
};
