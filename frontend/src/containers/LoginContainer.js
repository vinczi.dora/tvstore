import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/userActions'
import Login from "../components/Login";

class LoginContainer extends Component {
    render() {
        return (
            <Login {...this.props} />
        )}
}

const mapStateToProps = state => ({
    userData:state.userReducer
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators({...actions}, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginContainer)
