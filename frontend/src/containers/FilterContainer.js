import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/filterActions'
import Filter from '../components/Filter'
import FilterMultiselect from '../components/FilterMultiselect'
import FilterReset from '../components/FilterReset'
import ErrorMessage from '../components/ErrorMessage'

class FilterContainer extends Component {
    render() {
        return (
            <div>
                <Filter {...this.props} />
                <FilterMultiselect {...this.props}/>
                <FilterReset tvsFiltered={this.props.tvsFiltered} onClear={this.props.clearFilter}/>
                {this.props.error ? <ErrorMessage content={this.props.error} /> : ''}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    tvs: state.listReducer.tvs,
    ...state.filterReducer
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators(actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FilterContainer)
