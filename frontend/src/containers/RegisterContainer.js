import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/userActions'
import Register from "../components/Register";

class RegisterContainer extends Component {
    render() {
        return (
            <Register {...this.props} />
        )}
}

const mapStateToProps = state => ({
    userData : state.userReducer
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators({...actions}, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterContainer)
