import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getSingleTv } from '../actions/tvActions'
import SingleTv from '../components/SingleTv'

class SingleTvContainer extends Component {

    render() {
        return (
            <SingleTv {...this.props} />
        )
    }
}

const mapStateToProps = state => ({
    ...state.SingleTvReducer
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators({getSingleTv}, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(SingleTvContainer));
