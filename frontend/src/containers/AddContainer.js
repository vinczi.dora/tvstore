import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions/addNewActions'
import { getDevices } from '../actions/tvActions'
import AddForm from '../components/AddForm'

class AddContainer extends Component {
    componentDidMount() {
        this.props.getDevices()
    }

    render() {
        return (
            <AddForm {...this.props} />
        )}
}

const mapStateToProps = state => ({
    addNew: {
        displaySizeInInches: state.addReducer.displaySizeInInches,
        displayType: state.addReducer.displayType,
        resolutionK: state.addReducer.resolutionK,
        outputs: state.addReducer.outputs,
        name: state.addReducer.name,
        itemNo: state.addReducer.itemNo
    },
    existingIds: state.listReducer.tvs.map(tv => tv['itemNo'])
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators({...actions, getDevices}, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddContainer)
