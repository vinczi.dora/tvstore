import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getDevices } from '../actions/tvActions'
import { logout } from '../actions/userActions'
import List from '../components/List'
import Loader from '../components/Loader'
import LogoutButton from '../components/LogoutButton'

class ListContainer extends Component {
    componentDidMount () {
        this.props.getDevices()
    }

    render() {
        return (
            this.props.isLoading ? <Loader /> :
                <div>
                    <LogoutButton logout={this.props.logout} isLoggedIn={this.props.isLoggedIn}/>
                    <List tvsFiltered={this.props.tvsFiltered} tvs={this.props.tvs}/>
                </div>
        )
    }
}

const mapStateToProps = state => ({
    isLoading: state.loadingReducer.isLoading,
    tvsFiltered: state.filterReducer.tvsFiltered,
    isLoggedIn: state.userReducer.isLoggedIn,
    ...state.listReducer
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators({getDevices, logout}, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListContainer)
