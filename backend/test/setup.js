require('dotenv').config({path: '.env.test'});
const config = require('../config');

const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

beforeEach(function beforeEach() {
    this.sandbox = sinon.createSandbox();
});

afterEach(function afterEach() {
    this.sandbox.restore();
});
