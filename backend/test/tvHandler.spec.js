const Tv = require('../db/tv');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = require('chai');
const app = require('../web/app');
const { init } = require('../web/server');

/*describe('tv handler', () => {
    it('should call', () => {
        return request(app).get('/healthcheck')
            .expect(200)
            .expect('ok')
    });
    it('should call', () => {
        return request(app).get('/')
            .expect(200)
            .expect({message: 'ok'})
    });
});*/

chai.use(chaiHttp);
const url = '/tvs';
const allTvs = [
    {
        "_id": "5c83eb12b8b0d1dac8256b94",
        "displaySizeInInches": 140,
        "displayType": "led",
        "resolutionK": 4,
        "outputs": [
            "usb-micro",
            "usb-a",
            "analog",
            "hdmi",
            "coax"
        ],
        "name": "Philips ZEAX5",
        "itemNo": "ZWETFPUG0Z"
    },
    {
        "_id": "5c83eb12b8b0d1dac8256b95",
        "displaySizeInInches": 73,
        "displayType": "led",
        "resolutionK": 1,
        "outputs": [
            "hdmi",
            "usb-c"
        ],
        "name": "Panasonic 3F9JV",
        "itemNo": "12FSR4ZLQY"
    }
];

const allTvsPlusOne = [
    {
        "_id": "5c83eb12b8b0d1dac8256b94",
        "displaySizeInInches": 140,
        "displayType": "led",
        "resolutionK": 4,
        "outputs": [
            "usb-micro",
            "usb-a",
            "analog",
            "hdmi",
            "coax"
        ],
        "name": "Philips ZEAX5",
        "itemNo": "ZWETFPUG0Z"
    },
    {
        "_id": "5c83eb12b8b0d1dac8256b95",
        "displaySizeInInches": 73,
        "displayType": "led",
        "resolutionK": 1,
        "outputs": [
            "hdmi",
            "usb-c"
        ],
        "name": "Panasonic 3F9JV",
        "itemNo": "12FSR4ZLQY"
    },
    {
        "displaySizeInInches": 100,
        "displayType": "led",
        "resolutionK": 1,
        "outputs": [
            "usb-micro",
            "usb-a",
            "analog"
        ],
        "name": "TestTv 1000",
        "itemNo": "TESTTV0100"
    }
];

const oneTv = {
    "_id": "5c83eb12b8b0d1dac8256b94",
    "displaySizeInInches": 140,
    "displayType": "led",
    "resolutionK": 4,
    "outputs": [
        "usb-micro",
        "usb-a",
        "analog",
        "hdmi",
        "coax"
    ],
    "name": "Philips ZEAX5",
    "itemNo": "ZWETFPUG0Z"
};

describe('GET ${url}', () => {
    it('should return 200 and give back all tvs', async function() {
        this.sandbox.stub(Tv, 'list').resolves(allTvs);
        const agent = await chai.request.agent(app.listen());
        const response = await agent.get(url);
        expect(response).to.have.status(200);
        expect(response.body).to.eql(allTvs);
    });
});


describe('GET ${url}/:id', () => {
    it('should return 200 and give back one tv based on id', async function() {
        const id = '5c83eb12b8b0d1dac8256b94';
        this.sandbox.stub(Tv, 'get').withArgs(id).resolves(oneTv);
        const agent = await chai.request.agent(app.listen());
        const response = await agent.get(`/tvs/${id}`);
        expect(response).to.have.status(200);
        expect(response.body).to.eql(oneTv);
    });
});

/*
describe('POST ${url}', () => {
    it('should return 400 because incorrect submitted tv', async function() {
        this.sandbox.stub(Tv, 'insert').withArgs({}).resolves({});
        const agent = await chai.request.agent(app.listen());
        const response = await agent.post(url);
        expect(response).to.have.status(400);
        expect(response.body).to.eql({});
    });
});

describe('POST ${url}', () => {
    it('should return 200 and the inserted tv', async function() {
        const newTv = {
            "displaySizeInInches": 100,
            "displayType": "led",
            "resolutionK": 1,
            "outputs": [
                "usb-micro",
                "usb-a",
                "analog"
            ],
            "name": "TestTv 1000",
            "itemNo": "TESTTV0100"
        };
        this.sandbox.stub(Tv, 'insert').withArgs(newTv).resolves(newTv);
        const agent = await chai.request.agent(app.listen());
        const response = await agent.post(url);
        expect(response).to.have.status(200);
        expect(response.body).to.eql(newTv);
    });
});
*/

describe('GET /healthcheck', () => {
    it('should return 200', async function() {
        const agent = await chai.request.agent(app.listen());
        const response = await agent.get('/healthcheck');
        expect(response).to.have.status(200);
    });
});
// összes usecase (errorok, db hiba... megfelelő kód )