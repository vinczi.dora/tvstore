const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config');

async function jwtCookieAuth(req, res, next) {
  const token = req.cookies.token;

  if (!token) {
    return res.status(401).send({ message: 'Unauthorized, missing data' });
  }

  try {
    const data = jwt.verify(token, jwtSecret);
    const userEmail = data['email'];

  } catch (err) {
    console.log('Unauthorized, invalid token');
  }

  return next();
}

module.exports = jwtCookieAuth;
