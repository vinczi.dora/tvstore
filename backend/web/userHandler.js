const jwt = require('jsonwebtoken');
const users = require('../db/user');
const { jwtSecret } = require('../config')

async function register(req, res) {
  const user = await users.register(req.body);
  res.status(201);
  res.send(user);
}

async function loginJWTCookie(req, res) {
  const { email, password } = req.body;

  const user = await users.findByEmail(email);
  if (!user || password !== user.password) {
    return res.status(401).send({ message: 'Unauthorized, invalid login credentials' });
  } else {
    const token = jwt.sign({ email }, jwtSecret);
    res.cookie('token', token, {maxAge: 9000000000});
    res.send({user: user.email, token});
  }
}

async function logout(req, res) {
  res.clearCookie('token', {path: '/'});
  res.status(200);
  res.send('user logged out')
}

module.exports = {
  register,
  login: loginJWTCookie,
  logout
};
