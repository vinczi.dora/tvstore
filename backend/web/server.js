const http = require('http');
const { promisify } = require('util');
const { port } = require('../config');
const app = require('./app')

const server = http.createServer(app);

const listenPromise = promisify(server.listen.bind(server, port));
module.exports = {
  init: listenPromise,
};
