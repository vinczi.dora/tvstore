const { Router } = require('express');
const {
  get, list, insert, update, remove,
} = require('./tvHandler');
const { register, login, logout } = require('./userHandler');
const auth = require('../auth/jwtCookie');

const publicRouter = Router();
const privateRouter = Router();

privateRouter.use(auth);

publicRouter.get('/', (req, res) => {
  res.send({
    message: 'ok',
  });
});

publicRouter.get('/healthcheck', (req, res) => {
  res.send('ok');
});

// tv inventory related endpoints
privateRouter.get('/tvs', list);
privateRouter.get('/tvs/:id', get);
privateRouter.post('/tvs', insert);
privateRouter.put('/tvs/:id', update);
privateRouter.delete('/tvs/:id', remove);

// user related endpoints
publicRouter.post('/register', register);
publicRouter.post('/login', login);
publicRouter.get('/logout', logout);

publicRouter.post('/tvs', insert);

module.exports = {
  publicRouter,
  privateRouter,
};
