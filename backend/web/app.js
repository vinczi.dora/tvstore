const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = require('../config');
const { privateRouter, publicRouter } = require('./router');
const logger = require('../logger');

const app = express();

app.use(cors({"origin": [
    "http://localhost:4000",
    "http://localhost:4000/login",
    "http://localhost:3000",
    "http://localhost:3000/login",
    "http://localhost:3000/tvs",
    "http://localhost:4000/tvs"
  ],
  "allowedHeaders": ['Content-Type', 'Origin', 'X-Auth-Token'],
  "preflightContinue": true,
  "credentials": true}));
app.use(cookieParser());
app.use(bodyparser.json());

app.use((req, res, next) => {
  logger.debug(`${req.method} ${req.url} at ${new Date()}`);
  next();
});

app.use(publicRouter);
app.use(privateRouter);

app.use((err, req, res, next) => {
  logger.error(err);
  res.status(500);
  return res.send({
    errorCode: 1945,
    errorMessage: 'ez egy hiba',
  });
});

module.exports = app;
